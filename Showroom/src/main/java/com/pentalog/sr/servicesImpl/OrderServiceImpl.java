package com.pentalog.sr.servicesImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pentalog.sr.dao.OrderDAO;
import com.pentalog.sr.model.Order;
import com.pentalog.sr.service.OrderService;

/**
 * The class OrderDAO.
 */
@Service(value="orderService")
public class OrderServiceImpl implements OrderService{

	/**
	 * The order DAO.
	 */
	@Autowired
	private OrderDAO orderDao ;
	
	public OrderDAO getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDAO orderDao) {
		this.orderDao = orderDao;
	}

	@PersistenceContext
	EntityManager em;
	
	/**
	 * @see OrderService#getOrderById()
	 */
	@Override
	public Order getOrderById(int id) {
		Order returnedOrder =  this.orderDao.getOrderById(id);
		System.out.println(returnedOrder.getOrderId());
		return returnedOrder;
	}

	/**
	 * @see OrderService#getAllOrders()
	 */
	@Override
	public List<Order> getAllOrders() {
		List<Order> allOrders = this.orderDao.getAllOrders();
		for(Order o : allOrders)
			System.out.println(o.getOrderId()+" "+o.getOrderDate());
		return allOrders;
	}

	@Override
	public List<Order> getOrderByDate(Date date) {
		List<Order> returnedOrder = this.orderDao.getOrderByDate(date);
		return returnedOrder;
	}	
}

