package com.pentalog.sr.servicesImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pentalog.sr.dao.CustomerDAO;
import com.pentalog.sr.model.Customer;
import com.pentalog.sr.service.CustomerService;

/**
 * The class CustomerDAO.
 */
@Service(value="customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService{

	/**
	 * The customer DAO.
	 */
	@Autowired
	private CustomerDAO customerDao ;
	
	@PersistenceContext
	EntityManager em;
	
	/**
	 * @see CustomerService#getCustomerById()
	 */
	@Override
	public Customer getCustomerById(int id) {
		Customer returnedCustomer =  this.customerDao.getCustomerdById(id);
		System.out.println(returnedCustomer.getCustomerId());
		return returnedCustomer;
	}

	/**
	 * @see SomeInterface#someMethod()
	 */
	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Method that announces the buyer of the finished transaction
	 */
	public String anounceBuyer()
	{
		//TODO When the status is set to FINISHED
		return "announce-buyer?faces-redirect=true"; 
	}
	
	
}

