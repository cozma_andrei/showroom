package com.pentalog.sr.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * The Class GeneralSpringConfig.
 */
@Configuration
@ImportResource({"/WEB-INF/application-config.xml"})
@ComponentScan(basePackages = { "com.pentalog.sr.dao","com.pentalog.sr.service",
		})
public class GeneralSpringConfig {
	
	public GeneralSpringConfig(){
	}

	/**
	 * Register the PropertySourcesPlaceholderConfigurer.
	 * 
	 * @return PropertySourcesPlaceholderConfigurer instance
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}