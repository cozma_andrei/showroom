package com.pentalog.sr.service;

import java.util.List;
import com.pentalog.sr.model.Product;

/**
 * The interface for ProductService
 */
public interface ProductService {
	
	/**
	 * Get product by id.
	 * @param id - the id.
	 * @return product by id.
	 */
	Product getProductById(int id);
	
	/**
	 * Get product by price.
	 * @param price - the price.
	 * @return product by price.
	 */
	List<Product> getProductByPrice(double price);
	
	/**
	 * Get all products.
	 * @return products.
	 */
	List<Product> getAllProducts();
}
