package com.pentalog.sr.service;

import java.util.List;
import com.pentalog.sr.model.Customer;

/**
 * The interface CustomerService
 */
public interface CustomerService {
	
	/**
	 * Get customer by id.
	 * @param id - the id.
	 * @return customer by id.
	 */
	Customer getCustomerById(int id);
	
	/**
	 * Get all customers.
	 * @return customers.
	 */
	List<Customer> getAllCustomers();
}
