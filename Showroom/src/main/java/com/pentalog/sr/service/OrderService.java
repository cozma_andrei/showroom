package com.pentalog.sr.service;

import java.util.Date;
import java.util.List;

import com.pentalog.sr.model.Order;

/**
 * The interface OrderService.
 */
public interface OrderService {
	
	/**
	 * Get order by id.
	 * @param id - the id
	 * @return order by id.
	 */
	Order getOrderById(int id);
	
	/**
	 * Get orders by date.
	 * @param date - the date.
	 * @return orders by date.
	 */
	List<Order> getOrderByDate(Date date);
	
	/**
	 * Get all orders.
	 * @return orders.
	 */
	List<Order> getAllOrders();

}
