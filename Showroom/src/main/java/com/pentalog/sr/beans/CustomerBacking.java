package com.pentalog.sr.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;

import com.pentalog.sr.dao.CustomerDAO;
import com.pentalog.sr.dao.OrderDAO;
import com.pentalog.sr.model.Customer;
import com.pentalog.sr.model.Order;
import com.pentalog.sr.service.CustomerService;
import com.pentalog.sr.servicesImpl.OrderServiceImpl;

@ViewScoped
@ManagedBean(name = "customerBean")
public class CustomerBacking implements Serializable {

	private static final long serialVersionUID = 1L;
	Customer customer;

	@ManagedProperty(value = "#{customerService}")
	private CustomerService customerService;

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customerService.getCustomerById(0); // TODO set id from
															// page
	}
}
