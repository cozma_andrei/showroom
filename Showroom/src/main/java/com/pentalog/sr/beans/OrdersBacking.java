package com.pentalog.sr.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;


import com.pentalog.sr.dao.OrderDAO;
import com.pentalog.sr.model.Order;

import com.pentalog.sr.servicesImpl.OrderServiceImpl;

@ViewScoped
@ManagedBean(name = "orderBean")
public class OrdersBacking implements Serializable {

	/**
	 * The order DAO.
	 */
	@Autowired
	private OrderDAO orderDao;
	private static final long serialVersionUID = 1L;
	private List<Order> orders;

	@ManagedProperty(value = "#{orderService}")
	private OrderServiceImpl orderService;

	// @PostConstruct
	 public void init() {
	 orders = new ArrayList<>();
	 orders = orderService.getAllOrders();
	 }

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

}
