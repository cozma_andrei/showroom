package com.pentalog.sr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pentalog.sr.model.Order;

/**
 * The interface OrderDAO
 */
public interface OrderDAO extends JpaRepository<Order, Integer>{
	
	/**
	 * The constant GET_CUSTOMER_BY_ID.
	 */
	static final String GET_ORDER_BY_ID = "Select * from order where orderId = :orderId";
	
	static final String GET_ALL_ORDERS = "Select * from order";
	
	static final String GET_ORDER_BY_DATE = "Select * from order where orderDate = :orderDate";
	
	/**
	 * Get customer by id.
	 * @param id - the id.
	 * @return customer by id.
	 */
	@Query(GET_ORDER_BY_ID)
	public Order getOrderById(@Param("orderId") int id);
	
	@Query(GET_ALL_ORDERS)
	public List<Order> getAllOrders();
	
	@Query(GET_ORDER_BY_DATE)
	public List<Order> getOrderByDate(@Param("orderDate") Date d);
}