package com.pentalog.sr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pentalog.sr.model.Customer;

/**
 * The interface CostomerDAO.
 */
@Transactional
public interface CustomerDAO extends JpaRepository<Customer, Integer>{
	
	/**
	 * The constant GET_CUSTOMER_BY_ID.
	 */
	static final String GET_CUSTOMER_BY_ID = "Select * from Customer where id = :customerId";
	
	/**
	 * Get customer by id.
	 * @param id - the id.
	 * @return customer by id.
	 */
	@Query(GET_CUSTOMER_BY_ID)
	public Customer getCustomerdById(@Param("customerId") int id);
}