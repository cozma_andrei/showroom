package com.pentalog.sr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pentalog.sr.model.Product;

/**
 * The interface ProductDAO.
 */
public interface ProductDAO extends JpaRepository<Product, Integer> {}
