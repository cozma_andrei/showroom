package com.pentalog.sr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
@Controller
public class SpringController {
 
	//    @RequestMapping(value = "/jsfdemo", method = RequestMethod.GET)
	//    public String welcomePageForJSF(Model model) {
	//        model.addAttribute("message", "This is Welcome page for Java Server Faces (JSF)");
	//        model.addAttribute("number","1");
	//        return "indexJSF";
	//    }
	    @RequestMapping(value = "/order", method = RequestMethod.GET)
	    public String orderPage(Model model) {
	        return "orders";
	    }
}